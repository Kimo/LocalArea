package com.example.localareaapp;

import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class AnotherAccount extends Activity{

	String sourceID;

	String type;

	String id;
	String name;
	String email;

	private Button typeButton;
	private TextView friendName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_anotheraccount);

		Intent myIntent = getIntent(); // gets the previously created intent
		id = myIntent.getStringExtra("user_id");
		sourceID = myIntent.getStringExtra("sourceID");
		type = myIntent.getStringExtra("type");

		

		// /
		// name =
		// email =
		// /
		

		friendName = (TextView) findViewById(R.id.userName);

		String method = "getUserByID";

		BackgroundTask bt = new BackgroundTask(this);
		bt.execute(method, id);

		try {
			bt.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String[] users = bt.output.split("-");

		name = users[1];
		email = users[2];

//		Toast.makeText(this, sourceID + " " + id + " " + type,
//				Toast.LENGTH_LONG).show();
		
		typeButton = (Button) findViewById(R.id.addButton);
		typeButton.setText(type);
		typeButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Toast.makeText(getApplicationContext(), sourceID + " " + id + " " + type,
						Toast.LENGTH_LONG).show();
				
				if (type.equals("Add Friend"))
				{
					typeButton.setText("Sent Request");
				}
				else if (type.equals("Accept Friend"))
				{
					typeButton.setText("Friend");
				}
				
				if (type .equals("Accept Friend")||type.equals("Add Friend"))
				{
					// /background to add friend or accept
					String method = type;
					BackgroundTask bt = new BackgroundTask(getApplicationContext());
					bt.execute(method, id, sourceID);	
					
					try {
						bt.get();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ExecutionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
		});
		
		friendName = (TextView) findViewById(R.id.friendName);
		friendName.setText(name);
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.another_account, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
