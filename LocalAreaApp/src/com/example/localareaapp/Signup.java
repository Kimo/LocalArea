package com.example.localareaapp;

import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class Signup extends Activity implements OnClickListener {

	private Button supmit;
	private EditText username, password, email, creditTxt;
	private TextView ctxt;
	private RadioButton p_user, n_user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup);

		supmit = (Button) findViewById(R.id.supmitButton);
		supmit.setOnClickListener(this);

		username = (EditText) findViewById(R.id.usernameUp);

		password = (EditText) findViewById(R.id.passwordUp);

		email = (EditText) findViewById(R.id.emailUp);

		creditTxt = (EditText) findViewById(R.id.creditTxt);
		creditTxt.setVisibility(View.GONE);
		ctxt = (TextView) findViewById(R.id.creditTxtview);
		ctxt.setVisibility(View.GONE);

		p_user = (RadioButton) findViewById(R.id.Puser);
		p_user.setOnClickListener(this);

		n_user = (RadioButton) findViewById(R.id.Nuser);
		n_user.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {

		case R.id.supmitButton:
			String uname = username.getText().toString();
			String upassword = password.getText().toString();
			String uEmail = email.getText().toString();

			if (uname.length() == 0 || upassword.length() == 0
					|| uEmail.length() == 0) {
				Toast.makeText(this, "Please enter all data!",
						Toast.LENGTH_LONG).show();
			} else if (((RadioButton) p_user).isChecked()
					&& creditTxt.length() == 0) {
				Toast.makeText(this, "Please enter creditcard No..!",
						Toast.LENGTH_LONG).show();
			} else {
				String type = "";
				if (((RadioButton) n_user).isChecked()) {
					type += 1;
					String method = "signup";

					BackgroundTask bt = new BackgroundTask(this);
					bt.execute(method, uname, upassword, uEmail, type);

					Intent intent = new Intent(v.getContext(), Login.class);
					startActivityForResult(intent, 0);
				} else {
					String method = "verifyCreditcard";
					String no = creditTxt.getText().toString();
					premiumController btt = new premiumController(this);
					btt.execute(method, no);
					try {
						btt.get();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ExecutionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					if (btt.output.charAt(0) == '-') {
						Toast.makeText(this, "wrong creditcard number !!!",
								Toast.LENGTH_LONG).show();

					} else {

						type += 2;

						String method1 = "signup";

						BackgroundTask bt = new BackgroundTask(this);
						bt.execute(method1, uname, upassword, uEmail, type);

						Intent intent = new Intent(v.getContext(), Login.class);
						startActivityForResult(intent, 0);
					}
				}

			}
			break;

		case R.id.Puser:
			ctxt.setVisibility(1);
			creditTxt.setVisibility(1);
			if (((RadioButton) n_user).isChecked())
				n_user.setChecked(false);
			break;

		case R.id.Nuser:
			ctxt.setVisibility(View.GONE);
			creditTxt.setVisibility(View.GONE);
			if (((RadioButton) p_user).isChecked())
				p_user.setChecked(false);
			break;

		default:
			return;
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.signup, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
