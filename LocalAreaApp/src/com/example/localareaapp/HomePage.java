package com.example.localareaapp;

import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;



public class HomePage extends Activity implements OnClickListener {
	
	private String id;
	private String name;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_page);
		
		Intent myIntent = getIntent(); // gets the previously created intent
		id = myIntent.getStringExtra("user_id");
		
		Toast.makeText(this, id,
				Toast.LENGTH_LONG).show();
		
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home_page, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}else if (id == R.id.signout)
		{
			String method = "signout";
			BackgroundTask bt = new BackgroundTask(this);
			bt.execute(method, this.id+"");
			try {
				bt.get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Intent launchNewIntent = new Intent(HomePage.this,Login.class);
			startActivityForResult(launchNewIntent, 0);
		}
		else if (id == R.id.myAccount)
		{
			
			Intent intent = new Intent(HomePage.this,Account.class);
			intent.putExtra("user_id", this.id + "");
			startActivityForResult(intent, 0);
		}
		else if (id == R.id.search)
		{
			Intent intent = new Intent(HomePage.this,Search.class);
			intent.putExtra("sourceID", this.id + "");
			startActivityForResult(intent, 0);
		}
		return super.onOptionsItemSelected(item);
	}
}
