package com.example.localareaapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

public class BackgroundTask extends AsyncTask<String, Void, String> {

	Context ctx;
	public String id = new String();
	public String output = new String();

	public BackgroundTask(Context ctx) {
		super();
		this.ctx = ctx;
	}

	@Override
	protected void onPreExecute() {

	}

	@Override
	protected String doInBackground(String... params) {

		String signup_url = "http://localarea.netau.net/signup.php";
		String login_url = "http://localarea.netau.net/login.php";
		String signout_url = "http://localarea.netau.net/logout.php";
		String searchName_url = "http://localarea.netau.net/username_search.php";
		String getUserByID_url = "http://localarea.netau.net/userid_search.php";
		String addFriend_url = "http://localarea.netau.net/send_friend_request.php";
		String getRequests_url = "http://localarea.netau.net/get_current_requests.php";///////////////////////////////////////////////////////////////////////
		String acceptRequest_url = "http://localarea.netau.net/accept_friend_request.php";////////////////
		String bePremium_url = "http://localarea.netau.net/changeUserType.php";
		String verifyCreditcard_url = "http://localarea.netau.net/credit.php";/////////////////////////////////////////////////////////////////
		String userType_url = "http://localarea.netau.net/get_user_type.php";
		
		String method = params[0].toString();
		if (method == "signup") {
			try {

				String uname = params[1].toString();
				String upassword = params[2].toString();
				String uemail = params[3].toString();
				String utype = params[4].toString();

				// open a connection to the site
				URL url = new URL(signup_url);
				URLConnection con = url.openConnection();
				// activate the output
				con.setDoOutput(true);
				PrintStream ps = new PrintStream(con.getOutputStream());
				// send your parameters to your site
				ps.print("uname=" + uname);
				ps.print("&upassword=" + upassword);
				ps.print("&uEmail=" + uemail);
				ps.print("&utype=" + utype);

				// we have to get the input stream in order to actually send the
				// request
				con.getInputStream();
				// close the print stream
				ps.close();

				return "Regestration success...";

			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else if (method == "login") {

			try {
				String uname = params[1];
				String upassword = params[2];

				// open a connection to the site
				URL url = new URL(login_url);
				URLConnection con = url.openConnection();
				// activate the output
				con.setDoOutput(true);
				PrintStream ps = new PrintStream(con.getOutputStream());
				// send your parameters to your site
				ps.print("uname=" + uname);
				ps.print("&upassword=" + upassword);

				// we have to get the input stream in order to actually send the
				// request
				BufferedReader input = new BufferedReader(
						new InputStreamReader(con.getInputStream()));
				String line = "", res = "";
				while ((line = input.readLine()) != null) {
					res += line;
				}
				res = res.substring(0, res.indexOf("<"));
				// close the print stream
				ps.close();

				id = "" + res;

				if (res.charAt(0) == '-')
					return "Wrong username or password";
				return "*" + id;

			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (method == "signout") {
			String user_id = params[1].toString();
			try {
				URL url = new URL(signout_url);
				URLConnection con = url.openConnection();
				// activate the output
				con.setDoOutput(true);
				PrintStream ps = new PrintStream(con.getOutputStream());
				// send your parameters to your site
				ps.print("user_id=" + user_id);

				// we have to get the input stream in order to actually send the
				// request
				con.getInputStream();
				ps.close();

			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return "signout";
		} else if (method == "searchName") {
			try {
				String uname = params[1];

				// open a connection to the site
				URL url = new URL(searchName_url);
				URLConnection con = url.openConnection();
				// activate the output
				con.setDoOutput(true);
				PrintStream ps = new PrintStream(con.getOutputStream());
				// send your parameters to your site
				ps.print("uname=" + uname);

				// we have to get the input stream in order to actually send the
				// request
				BufferedReader input = new BufferedReader(
						new InputStreamReader(con.getInputStream()));
				String line = "", res = "";
				while ((line = input.readLine()) != null) {
					res += line;
				}
				res = res.substring(0, res.indexOf("<"));
				// close the print stream
				ps.close();

				// id = "" + res;
				output = res + "";

				return "Done";

			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (method == "getUserByID") {
			try {
				String uID = params[1];
				URL url = new URL(getUserByID_url);
				URLConnection con = url.openConnection();
				// activate the output
				con.setDoOutput(true);
				PrintStream ps = new PrintStream(con.getOutputStream());
				// send your parameters to your site
				ps.print("uid=" + uID);

				// we have to get the input stream in order to actually send the
				// request
				BufferedReader input = new BufferedReader(
						new InputStreamReader(con.getInputStream()));
				String line = "", res = "";
				while ((line = input.readLine()) != null) {
					res += line;
				}
				res = res.substring(0, res.indexOf("<"));
				// close the print stream
				ps.close();

				// id = "" + res;
				output = res + "";
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else if (method == "friendRequets") {
			String src = params[1];
			try {
				// open a connection to the site
				URL url = new URL(getRequests_url);
				URLConnection con = url.openConnection();
				// activate the output
				con.setDoOutput(true);
				PrintStream ps = new PrintStream(con.getOutputStream());
				// send your parameters to your site
				ps.print("user_id=" + src); ///////////////////////////////////

				// we have to get the input stream in order to actually send the
				// request
				BufferedReader input = new BufferedReader(
						new InputStreamReader(con.getInputStream()));
				String line = "", res = "";
				while ((line = input.readLine()) != null) {
					res += line;
				}
				res = res.substring(0, res.indexOf("<"));
				// close the print stream
				ps.close();

				// id = "" + res;
				output = res + "";
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (method.equals("Accept Friend")) {
			String src = params[1];
			String des = params[2];
			
			try {
				URL url = new URL(acceptRequest_url);
				URLConnection con = url.openConnection();
				// activate the output
				con.setDoOutput(true);
				PrintStream ps = new PrintStream(con.getOutputStream());
				// send your parameters to your site
				ps.print("source_id=" + src);
				ps.print("&destination_id=" + des);
				// we have to get the input stream in order to actually send the
				// request
				con.getInputStream();
				ps.close();

			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			////////////////////////////////////////
			
		} else if (method.equals("Add Friend")) {
			String src = params[1];
			String des = params[2];

			try {
				URL url = new URL(addFriend_url);
				URLConnection con = url.openConnection();
				// activate the output
				con.setDoOutput(true);
				PrintStream ps = new PrintStream(con.getOutputStream());
				// send your parameters to your site
				ps.print("source=" + src);
				ps.print("&destination=" + des);
				// we have to get the input stream in order to actually send the
				// request
				con.getInputStream();
				ps.close();

			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (method == "bePremium")
		{
			String src = params[1];

			try {
				URL url = new URL(bePremium_url);
				URLConnection con = url.openConnection();
				// activate the output
				con.setDoOutput(true);
				PrintStream ps = new PrintStream(con.getOutputStream());
				// send your parameters to your site
				ps.print("USER_ID=" + src);
				// we have to get the input stream in order to actually send the
				// request
				con.getInputStream();
				ps.close();
				
				return "Done";

			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
		}else if (method == "verifyCreditcard")
		{
			String no = params[1];
			try {
				// open a connection to the site
				URL url = new URL(verifyCreditcard_url);
				URLConnection con = url.openConnection();
				// activate the output
				con.setDoOutput(true);
				PrintStream ps = new PrintStream(con.getOutputStream());
				// send your parameters to your site
				ps.print("CARD_NUMBER=" + no); ///////////////////////////////////////ubloadiii

				// we have to get the input stream in order to actually send the
				// request
				BufferedReader input = new BufferedReader(
						new InputStreamReader(con.getInputStream()));
				String line = "", res = "";
				while ((line = input.readLine()) != null) {
					res += line;
				}
				res = res.substring(0, res.indexOf("<"));
				// close the print stream
				ps.close();

				// id = "" + res;
				output = res + "";
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else if (method.equals("getType"))
		{
			String src = params[1];
			try {
				// open a connection to the site
				URL url = new URL(userType_url);
				URLConnection con = url.openConnection();
				// activate the output
				con.setDoOutput(true);
				PrintStream ps = new PrintStream(con.getOutputStream());
				// send your parameters to your site
				ps.print("user_id=" + src); ///////////////////////////////////

				// we have to get the input stream in order to actually send the
				// request
				BufferedReader input = new BufferedReader(
						new InputStreamReader(con.getInputStream()));
				String line = "", res = "";
				while ((line = input.readLine()) != null) {
					res += line;
				}
				res = res.substring(0, res.indexOf("<"));
				// close the print stream
				ps.close();

				// id = "" + res;
				output = res + "";
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		

		return "signout";
	}

	@Override
	protected void onProgressUpdate(Void... values) {
		super.onProgressUpdate(values);
	}

	@Override
	protected void onPostExecute(String result) {

		if (!result.equals("signout")) {
			if (result.charAt(0) == '*') {
				id = result.substring(1);
				result = "HELLO XD";
			}

			Toast.makeText(ctx, result, Toast.LENGTH_LONG).show();
		}
	}

}
