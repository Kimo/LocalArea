package com.example.localareaapp;

import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class Account extends Activity {

	private String id;
	private String name = new String();
	private String email = new String();

	private TextView userName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_account);

		Intent myIntent = getIntent(); // gets the previously created intent
		id = myIntent.getStringExtra("user_id");

		Toast.makeText(this, id, Toast.LENGTH_LONG).show();

		userName = (TextView) findViewById(R.id.userName);

		String method = "getUserByID";

		BackgroundTask bt = new BackgroundTask(this);
		bt.execute(method, id);

		try {
			bt.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String[] users = bt.output.split("-");

		name = users[1];
		email = users[2];

		userName.setText(name);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.account, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		} else if (id == R.id.friendrequests) {
			Intent intent = new Intent(Account.this, FriendRequests.class);
			;
			intent.putExtra("user_id", this.id);
			startActivityForResult(intent, 0);
		} else if (id == R.id.bepremium) {
			
			String method1 = "getType";
			BackgroundTask btt = new BackgroundTask(this);
			btt.execute(method1, this.id);

			try {
				btt.get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String type = btt.output;
///////////////////////////////////////////////////////////
			if (type.charAt(0) == '1') {
				String method = "bePremium";
				NormalController bt = new NormalController(this);
				bt.execute(method, this.id);

				try {
					bt.get();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				Toast.makeText(this, "You are already premium", Toast.LENGTH_LONG).show(); // 
			}

		}

		return super.onOptionsItemSelected(item);
	}
}
