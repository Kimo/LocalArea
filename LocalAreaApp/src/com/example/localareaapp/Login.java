package com.example.localareaapp;

import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends Activity implements OnClickListener {

	private Button login, signup, forgetPassword;
	private EditText username, password;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		login = (Button) findViewById(R.id.loginButton);
		login.setOnClickListener(this);

		signup = (Button) findViewById(R.id.signupButton);
		signup.setOnClickListener(this);

		forgetPassword = (Button) findViewById(R.id.forgetButton);
		forgetPassword.setOnClickListener(this);

		username = (EditText) findViewById(R.id.usernameText);

		password = (EditText) findViewById(R.id.passwordText);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.loginButton:

			if (username.length() == 0 || password.length() == 0) {
				Toast.makeText(this, "Enter username and password",
						Toast.LENGTH_LONG).show();
			} else {
				String uname = username.getText().toString();
				String upassword = password.getText().toString();
				String method = "login";

				BackgroundTask bt = new BackgroundTask(this);
				bt.execute(method, uname, upassword);
				
				try {
					bt.get();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				int user_id = Integer.parseInt(bt.id);

				if (user_id != -1) {
					Intent intent = new Intent(v.getContext(), HomePage.class);
					intent.putExtra("user_id", Integer.toString(user_id));
					startActivityForResult(intent, 0);
				}
			}
			break;

		case R.id.forgetButton:
			// /////////////////////////////////////////////
			break;

		case R.id.signupButton:
			Intent intent = new Intent(v.getContext(), Signup.class);
			startActivityForResult(intent, 0);
			// //////////////////////////////////////////////
			break;

		default:
			return;
		}

	}

	// ////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
