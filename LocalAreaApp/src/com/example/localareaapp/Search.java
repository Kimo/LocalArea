package com.example.localareaapp;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

public class Search extends Activity {

	private String sourceID; // want to sent to another account activity
	private EditText searchTxt;
	private Button search;
	private RadioButton name, place, brand;
	private ListView searchList;
	private ArrayAdapter<String> adapter;
	private ArrayList<String> uname;
	private ArrayList<String> uID;
	private ArrayList<String> uemail;

	private void nameSearch() { // fill array list name ,id ,email
		uname.clear();
		uID.clear();
		uemail.clear();

		String method = "searchName";
		String _uname = searchTxt.getText().toString();

		BackgroundTask bt = new BackgroundTask(this);
		bt.execute(method, _uname);

		try {
			bt.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String temp = bt.output;
		String[] users = temp.split("~");
		if (users[0].length() > 0)
			for (int i = 0; i < users.length; i++) {
				String[] str = users[i].split("-");
				uID.add(str[0]);
				uname.add(str[1]);
				uemail.add(str[2]);
			}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);

		Intent myIntent = getIntent(); // gets the previously created intent
		sourceID = myIntent.getStringExtra("sourceID");

		Toast.makeText(this, sourceID, Toast.LENGTH_LONG).show();

		searchTxt = (EditText) findViewById(R.id.searchText);
		search = (Button) findViewById(R.id.searchButton);
		name = (RadioButton) findViewById(R.id.nameRadio);
		name.setChecked(true);
		place = (RadioButton) findViewById(R.id.placeRadio);
		brand = (RadioButton) findViewById(R.id.brandRadio);
		searchList = (ListView) findViewById(R.id.searchList);

		uname = new ArrayList<String>();
		uID = new ArrayList<String>();
		uemail = new ArrayList<String>();

		uname.add("Empty List");

		adapter = new ArrayAdapter<String>(getApplicationContext(),
				android.R.layout.simple_list_item_1, uname);
		searchList.setAdapter(adapter);

		// //////////////////////////////
		searchList
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub

						if (position >= 0) {
							String t = "Add Friend";
							if (uname.get(position) != "Empty List") {
								Intent intent = new Intent(view.getContext(),
										AnotherAccount.class);
								intent.putExtra("user_id", uID.get(position));
								intent.putExtra("sourceID", sourceID);
								intent.putExtra("type", t);
								startActivityForResult(intent, 0);
							}
						}
					}

				});
		// //////////////////////////////////
		search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (searchTxt.getText().length() == 0) {
					Toast.makeText(getApplicationContext(),
							"Enter text to search on it", Toast.LENGTH_LONG)
							.show();
				} else {
					nameSearch();
					if (uname.size() == 0) {
						Toast.makeText(getApplicationContext(), "Not Found",
								Toast.LENGTH_LONG).show();
					} else
						adapter.notifyDataSetChanged();
				}
			}
		});
		// //////////////////////////////
		name.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				place.setChecked(false);
				brand.setChecked(false);
			}
		});
		// //////////////////////////////
		place.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				name.setChecked(false);
				brand.setChecked(false);
			}
		});
		// ////////////////////////////////////
		brand.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				place.setChecked(false);
				name.setChecked(false);
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.search, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
