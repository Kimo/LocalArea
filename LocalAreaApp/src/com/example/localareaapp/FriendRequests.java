package com.example.localareaapp;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class FriendRequests extends Activity {

	private String sourceID;

	private ListView requests;
	private ArrayAdapter<String> adapter;

	private ArrayList<String> uname;
	private ArrayList<String> uID;

	private void fillLists() { // fill array list name ,id ,email
		uname.clear();
		uID.clear();

		String method = "friendRequets";
		String _uID = sourceID;

		BackgroundTask bt = new BackgroundTask(this);
		bt.execute(method, _uID);

		try {
			bt.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String temp = bt.output;

		String[] users = temp.split("~");
		if (users[0].length() > 0)
			for (int i = 0; i < users.length; i++) {
				String[] data = users[i].split("-");

				uname.add(data[1]);
				uID.add(data[0]);
			}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_friend_requests);

		Intent myIntent = getIntent(); // gets the previously created intent
		sourceID = myIntent.getStringExtra("user_id");

		requests = (ListView) findViewById(R.id.friendRequests);
		uname = new ArrayList<String>();
		uID = new ArrayList<String>();

		adapter = new ArrayAdapter<String>(getApplicationContext(),
				android.R.layout.simple_list_item_1, uname);
		requests.setAdapter(adapter);

		fillLists();

		if (uname.size() == 0)
			uname.add("No Friend Requests");

		adapter.notifyDataSetChanged();
		// ////////////////////////////////////////
		requests.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				if (position >= 0) {
					String t = "Accept Friend";
					if (uname.get(position) != "No Friend Requests") {
						Intent intent = new Intent(view.getContext(),
								AnotherAccount.class);
						intent.putExtra("user_id", uID.get(position));
						intent.putExtra("sourceID", sourceID);
						intent.putExtra("type", t);
						startActivityForResult(intent, 0);
					}
				}
			}

		});
		// /////////////////////////////////
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.friend_requests, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
