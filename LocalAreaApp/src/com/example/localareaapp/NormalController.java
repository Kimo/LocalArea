package com.example.localareaapp;

import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.content.Context;

public class NormalController extends BackgroundTask{

	public NormalController(Context ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	protected String doInBackground(String... params) {
//////////////
		String bePremium_url = "http://localarea.netau.net/changeUserType.php";
		
		String method = params[0].toString();
		if (method == "bePremium")
		{
			String src = params[1];

			try {
				URL url = new URL(bePremium_url);
				URLConnection con = url.openConnection();
				// activate the output
				con.setDoOutput(true);
				PrintStream ps = new PrintStream(con.getOutputStream());
				// send your parameters to your site
				ps.print("USER_ID=" + src);
				// we have to get the input stream in order to actually send the
				// request
				con.getInputStream();
				ps.close();
				
				return "Done";

			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
		}
		
		

		return "signout";
	}

}
